# Tracking different sources

Allow zion to track sentinels, sniffers and loopholes in an uniform way,
make sure to have your redis running

```bash
bundle exec sidekiq -r ./lib/tracker/worker.rb ./lib/tracker/source_worker.rb
ruby app.rb passphrase
```
