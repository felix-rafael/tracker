require 'optparse'

require './lib/tracker'

options = {}
optparse = OptionParser.new do |opts|
  opts.banner = 'Usage: ruby app.rb [options]'

  opts.on('-p', '--passphrase PASSPHRASE', 'Passphrase required to identify yourself') do |p|
    options[:passphrase] = p
  end
end

optparse.parse!
passphrase = options[:passphrase]

unless passphrase
  puts 'missing --passphrase argument'
  puts optparse
  exit 1
end

tracker = Tracker.new(passphrase)
tracker.start
