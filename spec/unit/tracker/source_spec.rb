require 'spec_helper'

require './lib/tracker/source'

RSpec.describe Tracker::Source do
  it 'sends the data to the remote server' do
    fake_http = double('HTTP')
    expect(Net::HTTP).to receive(:new).and_return fake_http

    fake_request = double('Request')
    expect(Net::HTTP::Post).to receive(:new).and_return fake_request
    expect(fake_request).to receive(:set_form_data).with({ 'passphrase' => 'secret', 'source' => 'sentinels' })

    expect(fake_http).to receive(:request).with(fake_request).and_return double('Response', kind_of?: true)

    described_class.create('secret', { 'source' => 'sentinels' })
  end

  it 'fails if the response code is not 200' do
    allow_any_instance_of(Net::HTTP).to receive(:request).and_return double('Response', kind_of?: false, body: 'fail')

    expect {
      described_class.create('secret', {})
    }.to raise_error(Tracker::Source::RemoteServerError)
  end
end
