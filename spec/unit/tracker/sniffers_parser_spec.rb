require 'spec_helper'

require './lib/tracker/sniffers_parser'

RSpec.describe Tracker::SniffersParser do
  it 'parses the given result and return the routes' do
    content = {
      "sniffers/node_times.csv"=> File.read('./spec/fixtures/node_times.csv'),
      "sniffers/routes.csv"=> File.read('./spec/fixtures/routes.csv'),
      "sniffers/sequences.csv"=> File.read('./spec/fixtures/sequences.csv')
    }

    routes = described_class.parse(content)

    expect(routes.size).to eq(5)

    route0 = routes[0]
    expect(route0[:start_node]).to eq('lambda')
    expect(route0[:end_node]).to eq('tau')
    expect(route0[:start_time]).to eq('2030-12-31T13:00:06')
    expect(route0[:end_time]).to eq('2030-12-31T13:00:07')

    route1 = routes[1]
    expect(route1[:start_node]).to eq('tau')
    expect(route1[:end_node]).to eq('psi')
    expect(route1[:start_time]).to eq('2030-12-31T13:00:06')
    expect(route1[:end_time]).to eq('2030-12-31T13:00:07')

    route2 = routes[2]
    expect(route2[:start_node]).to eq('psi')
    expect(route2[:end_node]).to eq('omega')
    expect(route2[:start_time]).to eq('2030-12-31T13:00:06')
    expect(route2[:end_time]).to eq('2030-12-31T13:00:07')

    route3 = routes[3]
    expect(route3[:start_node]).to eq('lambda')
    expect(route3[:end_node]).to eq('psi')
    expect(route3[:start_time]).to eq('2030-12-31T13:00:07')
    expect(route3[:end_time]).to eq('2030-12-31T13:00:08')

    route4 = routes[4]
    expect(route4[:start_node]).to eq('psi')
    expect(route4[:end_node]).to eq('omega')
    expect(route4[:start_time]).to eq('2030-12-31T13:00:07')
    expect(route4[:end_time]).to eq('2030-12-31T13:00:08')
  end
end
