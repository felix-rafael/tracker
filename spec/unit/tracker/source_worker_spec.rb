require 'spec_helper'

require './lib/tracker/source_worker'

RSpec.describe Tracker::SourceWorker do
  it 'requests the source to be created' do
    expect(Tracker::Source).to receive(:create).with('secret', { 'source' => 'sentinels' })
    described_class.new.perform('secret', { 'source' => 'sentinels' })
  end
end
