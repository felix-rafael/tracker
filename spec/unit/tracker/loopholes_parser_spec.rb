require 'spec_helper'

require './lib/tracker/loopholes_parser'

RSpec.describe Tracker::LoopholesParser do
  it 'parses the given data and return the parsed routes' do
    content = {
      "loopholes/node_pairs.json"=> File.read('./spec/fixtures/node_pairs.json'),
      "loopholes/routes.json"=> File.read('./spec/fixtures/routes.json')
    }

    routes = described_class.parse(content)

    expect(routes.size).to eq(4)

    route0 = routes[0]
    expect(route0[:start_node]).to eq('gamma')
    expect(route0[:end_node]).to eq('theta')
    expect(route0[:start_time]).to eq('2030-12-31T13:00:04')
    expect(route0[:end_time]).to eq('2030-12-31T13:00:05')

    route1 = routes[1]
    expect(route1[:start_node]).to eq('theta')
    expect(route1[:end_node]).to eq('lambda')
    expect(route1[:start_time]).to eq('2030-12-31T13:00:05')
    expect(route1[:end_time]).to eq('2030-12-31T13:00:06')

    route2 = routes[2]
    expect(route2[:start_node]).to eq('beta')
    expect(route2[:end_node]).to eq('theta')
    expect(route2[:start_time]).to eq('2030-12-31T13:00:05')
    expect(route2[:end_time]).to eq('2030-12-31T13:00:06')

    route3 = routes[3]
    expect(route3[:start_node]).to eq('theta')
    expect(route3[:end_node]).to eq('lambda')
    expect(route3[:start_time]).to eq('2030-12-31T13:00:06')
    expect(route3[:end_time]).to eq('2030-12-31T13:00:07')
  end
end
