require 'spec_helper'

require './lib/tracker/sentinels_parser'

RSpec.describe Tracker::SentinelsParser do
  it 'parses the given result and save the routes' do
    contents = { "sentinels/routes.csv"=> File.read('./spec/fixtures/sentinels.csv') }

    routes = described_class.parse(contents)

    expect(routes.size).to eq(4)

    route0 = routes[0]
    expect(route0[:start_node]).to eq('gamma')
    expect(route0[:end_node]).to eq('beta')
    expect(route0[:start_time]).to eq('2030-12-31T16:00:04')
    expect(route0[:end_time]).to eq('2030-12-31T18:00:03')

    route1 = routes[1]
    expect(route1[:start_node]).to eq('beta')
    expect(route1[:end_node]).to eq('delta')
    expect(route1[:start_time]).to eq('2030-12-31T18:00:03')
    expect(route1[:end_time]).to eq('2030-12-31T22:00:02')

    route2 = routes[2]
    expect(route2[:start_node]).to eq('gamma')
    expect(route2[:end_node]).to eq('beta')
    expect(route2[:start_time]).to eq('2030-12-31T16:00:03')
    expect(route2[:end_time]).to eq('2030-12-31T18:00:02')

    route3 = routes[3]
    expect(route3[:start_node]).to eq('beta')
    expect(route3[:end_node]).to eq('alpha')
    expect(route3[:start_time]).to eq('2030-12-31T18:00:02')
    expect(route3[:end_time]).to eq('2030-12-31T22:00:01')
  end
end
