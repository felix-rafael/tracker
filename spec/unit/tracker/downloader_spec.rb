require 'spec_helper'

require './lib/tracker/downloader'

RSpec.describe Tracker::Downloader do
  it 'downloads the content and save to a tempfile' do
    uri = URI("#{Tracker::Downloader::BASE_URL}?passphrase=secret&source=sentinels")
    expect(Net::HTTP).to receive(:get).with(uri).and_return 'httpcontent'

    fake_file = double('FakeFile')
    expect(Tempfile).to receive(:new).with('sentinels.zip').and_return fake_file
    expect(fake_file).to receive(:write).with('httpcontent')
    expect(fake_file).to receive(:rewind)
    expect(fake_file).to receive(:close)
    expect(fake_file).to receive(:path).and_return 'path/to/file'

    downloader = Tracker::Downloader.new('secret', :sentinels)
    expect(downloader.download).to eq 'path/to/file'
  end
end
