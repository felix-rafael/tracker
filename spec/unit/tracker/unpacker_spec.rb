require 'spec_helper'

require './lib/tracker/unpacker'

RSpec.describe Tracker::Unpacker do
  it 'unpacks the given file and returns its contents' do
    real_file = double(name: 'realfile', file?: true)
    zip_content = [
      double(file?: false),
      double(name: '__MACOSX/something', file?: true),
      real_file
    ]
    expect(Zip::File).to receive(:open).with('path/to/zip').and_yield zip_content
    expect(real_file).to receive_message_chain(:get_input_stream, :read).and_return 'content'

    unpacker = described_class.new('path/to/zip')
    expect(unpacker.unpack).to eq('realfile' => 'content')
  end

  it 'unpacks the given file and returns its content for multiple files' do
    real_file = double(name: 'realfile', file?: true)
    other_file = double(name: 'anotherfile', file?: true)
    zip_content = [
      double(file?: false),
      double(name: '__MACOSX/something', file?: true),
      real_file,
      other_file
    ]
    expect(Zip::File).to receive(:open).with('path/to/zip').and_yield zip_content
    expect(real_file).to receive_message_chain(:get_input_stream, :read).and_return 'content'
    expect(other_file).to receive_message_chain(:get_input_stream, :read).and_return 'other content'

    unpacker = described_class.new('path/to/zip')
    expect(unpacker.unpack).to eq('realfile' => 'content', 'anotherfile' => 'other content')
  end
end
