require 'spec_helper'
require 'sidekiq/testing'

require './lib/tracker/worker'

describe Tracker::Worker do
  before do
    Sidekiq::Testing.fake!
  end

  it 'downloads and unpack the content from remote server' do
    expect(Tracker::Downloader).to receive(:download).with('secret', :sentinels).and_return 'path/to/file'
    expect(Tracker::Unpacker).to receive(:unpack).with('path/to/file').and_return('sentinels/routes.csv' => 'source_data')

    described_class.new.perform('secret', :sentinels)
  end

  context 'when is a sentinels tracker' do
    it 'sends the data to the sentinels parser and sends the results to be saved' do
      allow(Tracker::Downloader).to receive(:download).and_return 'path/to/file'
      allow(Tracker::Unpacker).to receive(:unpack).and_return 'source_data'

      expect(Tracker::SentinelsParser).to receive(:parse)
        .with('source_data')
        .and_return([{ 'source' => 'sentinels' }])

      expect {
        described_class.new.perform('secret', :sentinels)
      }.to change(Tracker::SourceWorker.jobs, :size).by(1)
    end
  end

  context 'when is a sniffers tracker' do
    it 'sends the data to the sniffers parser' do
      allow(Tracker::Downloader).to receive(:download).and_return 'path/to/file'
      allow(Tracker::Unpacker).to receive(:unpack).and_return 'source_data'

      expect(Tracker::SniffersParser).to receive(:parse)
        .with('source_data')
        .and_return([{ 'source' => 'sniffers' }])

      expect {
        described_class.new.perform('secret', :sniffers)
      }.to change(Tracker::SourceWorker.jobs, :size).by(1)
    end
  end

  context 'when is a loopholes tracker' do
    it 'sends the data to the loopholes parser' do
      allow(Tracker::Downloader).to receive(:download).and_return 'path/to/file'
      allow(Tracker::Unpacker).to receive(:unpack).and_return 'source_data'

      expect(Tracker::LoopholesParser).to receive(:parse)
        .with('source_data')
        .and_return([{ 'source' => 'loopholes' }])

      expect {
        described_class.new.perform('secret', :loopholes)
      }.to change(Tracker::SourceWorker.jobs, :size).by(1)
    end
  end
end
