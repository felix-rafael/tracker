require 'spec_helper'
require 'sidekiq/testing'

require './lib/tracker'

RSpec.describe Tracker do
  before do
    Sidekiq::Testing.fake!
  end

  describe 'starting source tracking' do
    let(:tracker) { Tracker.new('secret')  }

    it 'initializes the tracker workers' do
      expect(Tracker::Worker).to receive(:perform_async).with('secret', :sentinels).and_call_original
      expect(Tracker::Worker).to receive(:perform_async).with('secret', :sniffers).and_call_original
      expect(Tracker::Worker).to receive(:perform_async).with('secret', :loopholes).and_call_original

      expect {
        tracker.start
      }.to change(Tracker::Worker.jobs, :size).by(3)
    end
  end
end
