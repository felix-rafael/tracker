require 'zip'

class Tracker
  class Unpacker
    def self.unpack(path_to_file)
      new(path_to_file).unpack
    end

    def initialize(path_to_file)
      @path_to_file = path_to_file
    end

    def unpack
      content = {}

      Zip::File.open(@path_to_file) do |zip|
        zip.each do |entry|
          next if !entry.file? || entry.name.include?('__MACOSX')

          content[entry.name] = entry.get_input_stream.read
        end
      end

      content
    end
  end
end
