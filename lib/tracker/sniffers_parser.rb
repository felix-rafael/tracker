require 'csv'
require 'time'

class Tracker
  class SniffersParser
    def self.parse(content)
      new(content).parse
    end

    def initialize(content)
      @sequences = []

      CSV.parse(content.fetch('sniffers/sequences.csv'), headers: true, col_sep: ', ') do |row|
        @sequences << row.to_hash
      end

      @routes = []
      CSV.parse(content.fetch('sniffers/routes.csv'), headers: true, col_sep: ', ') do |row|
        @routes << row.to_hash
      end
      @routes = @routes.group_by { |route| route['route_id']  }

      @times = []
      CSV.parse(content.fetch('sniffers/node_times.csv'), headers: true, col_sep: ', ') do |row|
        @times << row.to_hash
      end
      @times = @times.group_by { |time| time['node_time_id'] }
    end

    def parse
      sniffers_routes = []

      @sequences.each do |seq|
        route = @routes[seq['route_id']]
        next unless route

        time = @times[seq['node_time_id']]
        next unless time

        route = route.first
        time = time.first

        start_time = Time.parse(route['time'])
        sniffer_route = {
          source: 'sniffers',
          start_node: time['start_node'],
          end_node: time['end_node'],
          start_time: start_time.strftime('%Y-%m-%dT%H:%M:%S'),
          end_time: (start_time + (time['duration_in_milliseconds'].to_i/1000)).strftime('%Y-%m-%dT%H:%M:%S')
        }

        sniffers_routes << sniffer_route
      end

      sniffers_routes
    end
  end
end
