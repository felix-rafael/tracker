require 'sidekiq'

require './lib/tracker/source'

class Tracker
  class SourceWorker
    include Sidekiq::Worker

    def perform(passphrase, data)
      Tracker::Source.create(passphrase, data)
    end
  end
end
