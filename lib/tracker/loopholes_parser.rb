require 'json'
require 'time'

class Tracker
  class LoopholesParser
    def self.parse(content)
      new(content).parse
    end

    def initialize(content)
      @nodes = JSON(content.fetch('loopholes/node_pairs.json'))['node_pairs']

      @routes = JSON(content.fetch('loopholes/routes.json'))['routes']
    end

    def parse
      loopholes_routes = []

      @routes.each do |route|
        node = find_node(route['node_pair_id'])

        next unless node

        loopholes_routes << {
          source: 'loopholes',
          start_node: node['start_node'],
          end_node: node['end_node'],
          start_time: Time.parse(route['start_time']).strftime('%Y-%m-%dT%H:%M:%S'),
          end_time: Time.parse(route['end_time']).strftime('%Y-%m-%dT%H:%M:%S')
        }
      end

      loopholes_routes
    end

    private

    def find_node(node_pair_id)
      @nodes.each do |node|
        return node if node['id'] == node_pair_id
      end

      nil
    end
  end
end
