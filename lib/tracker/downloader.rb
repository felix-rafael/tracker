require 'net/http'
require 'uri'
require 'tempfile'

class Tracker
  class Downloader
    BASE_URL = "http://challenge.distribusion.com/the_one/routes"

    def self.download(passphrase, source_type)
      new(passphrase, source_type).download
    end

    def initialize(passphrase, source_type)
      @passphrase = passphrase
      @source_type = source_type
    end

    def download
      http_result = Net::HTTP.get(request_uri)

      create_tempfile(http_result)
    end

    private

    def request_uri
      URI("#{BASE_URL}?#{params}")
    end

    def params
      {
        passphrase: @passphrase,
        source: @source_type
      }.map { |k, v| "#{k}=#{v}" }.join("&")
    end

    def create_tempfile(content)
      tempfile = Tempfile.new("#{@source_type}.zip")
      tempfile.write(content)
      tempfile.rewind
      tempfile.close

      tempfile.path
    end
  end
end
