require 'sidekiq'

require './lib/tracker/downloader'
require './lib/tracker/unpacker'
require './lib/tracker/sentinels_parser'
require './lib/tracker/sniffers_parser'
require './lib/tracker/loopholes_parser'
require './lib/tracker/source_worker'

class Tracker
  class Worker
    include Sidekiq::Worker

    def perform(passphrase, source_type)
      downloaded_file = Tracker::Downloader.download(passphrase, source_type)
      content = Tracker::Unpacker.unpack(downloaded_file)

      sources = case source_type.to_sym
        when :sentinels
          Tracker::SentinelsParser.parse(content)
        when :sniffers
          Tracker::SniffersParser.parse(content)
        when :loopholes
          Tracker::LoopholesParser.parse(content)
        else
          []
        end

      sources.each do |source|
        Tracker::SourceWorker.perform_async(passphrase, source)
      end
    end
  end
end
