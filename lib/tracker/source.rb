require 'net/http'
require 'uri'

class Tracker
  class Source
    RemoteServerError = Class.new(StandardError)

    URL = 'http://challenge.distribusion.com/the_one/routes'

    def self.create(passphrase, data)
      new(passphrase).create(data)
    end

    def initialize(passphrase)
      @passphrase = passphrase
    end

    def create(data)
      uri = URI.parse(URL)

      http = Net::HTTP.new(uri.host, uri.port)

      request = Net::HTTP::Post.new(uri.request_uri)
      request.set_form_data(data.merge('passphrase' => @passphrase))

      response = http.request(request)

      raise RemoteServerError.new(response.body) unless response.kind_of?(Net::HTTPSuccess)
    end
  end
end
