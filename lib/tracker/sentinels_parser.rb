require 'time'
require 'csv'

class Tracker
  class SentinelsParser
    def self.parse(contents)
      new(contents).parse
    end

    def initialize(contents)
      csv_content = contents.fetch('sentinels/routes.csv')
      @rows = []

      CSV.parse(csv_content, headers: true, col_sep: ', ') do |row|
        route = row.to_hash
        route['time'] = Time.parse(route['time'])

        @rows << route
      end

      @rows.sort! { |a, b| b['time'] <=> a['time'] }
    end

    def parse
      sentinel_routes = []

      @rows.group_by { |route| route['route_id'] }.each do |_, routes|
        previous_route = nil

        next if routes.size == 1

        routes.each do |route|
          if previous_route
            sentinel_route = {
              source: 'sentinels',
              start_node: previous_route['node'],
              end_node: route['node'],
              start_time: previous_route['time'].strftime('%Y-%m-%dT%H:%M:%S'),
              end_time: route['time'].strftime('%Y-%m-%dT%H:%M:%S')
            }

            sentinel_routes << sentinel_route
          end

          previous_route = route
        end
      end

      sentinel_routes
    end
  end
end
