require './lib/tracker/worker'

class Tracker
  def initialize(passphrase)
    @passphrase = passphrase
  end

  def start
    Tracker::Worker.perform_async(@passphrase, :sentinels)
    Tracker::Worker.perform_async(@passphrase, :sniffers)
    Tracker::Worker.perform_async(@passphrase, :loopholes)
  end
end
